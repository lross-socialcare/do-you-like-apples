package lross.doyoulikeapples.web;

import lombok.RequiredArgsConstructor;
import lross.doyoulikeapples.domain.Apple;
import lross.doyoulikeapples.service.AppleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
public class AppleController {

    private final AppleService appleService;

    @RequestMapping("/")
    public Collection<Apple> getApples(@RequestParam String appleVariety) {

        return appleService.getApplesByVariety(appleVariety);
    }
}
