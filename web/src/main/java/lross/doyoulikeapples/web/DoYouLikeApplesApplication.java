package lross.doyoulikeapples.web;

import lross.doyoulikeapples.service.DoYouLikeApplesServiceSpringConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({DoYouLikeApplesServiceSpringConfiguration.class})
public class DoYouLikeApplesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoYouLikeApplesApplication.class);
    }
}
