package lross.doyoulikeapples.clientokhttp;

import com.fasterxml.jackson.databind.ObjectMapper;
import lross.doyoulikeapples.domain.Apple;
import lross.doyoulikeapples.web.DoYouLikeApplesApplication;
import okhttp3.OkHttpClient;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = {DoYouLikeApplesApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppleClientIT {

    private @LocalServerPort int port;
    private AppleClient appleClient;

    @Before
    public void before() {
        appleClient = new AppleClient(new OkHttpClient(), new ObjectMapper(), "localhost", port);
    }

    @Test
    public void appleClientShouldReturnApples() {

        Collection<Apple> apples = appleClient.getApplesByVariety("Braeburn");
        Assertions.assertThat(apples).containsExactly(new Apple("Braeburn"));
    }
}
