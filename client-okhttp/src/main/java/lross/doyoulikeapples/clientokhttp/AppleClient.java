package lross.doyoulikeapples.clientokhttp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lross.doyoulikeapples.api.AppleApi;
import lross.doyoulikeapples.domain.Apple;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;

@Component
@RequiredArgsConstructor
public class AppleClient implements AppleApi {

    private final OkHttpClient client;
    private final ObjectMapper mapper;
    private final String hostname;
    private final int port;

    @Override
    public Collection<Apple> getApplesByVariety(String variety) {

        Request request = new Request.Builder()
                .url(new HttpUrl.Builder()
                        .scheme("http")
                        .host(hostname)
                        .port(port)
                        .addQueryParameter("appleVariety", variety)
                        .build())
                .build();
        try {
            Response response = client.newCall(request).execute();
            return mapper.readValue(response.body().string(), new TypeReference<Collection<Apple>>() {});
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
