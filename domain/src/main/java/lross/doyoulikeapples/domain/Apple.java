package lross.doyoulikeapples.domain;

import lombok.Data;

@Data
public class Apple {

    private final String variety;
}
