package lross.doyoulikeapples.service;

import lross.doyoulikeapples.api.AppleApi;
import lross.doyoulikeapples.domain.Apple;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
public class AppleService implements AppleApi {

    @Override
    public Collection<Apple> getApplesByVariety(String variety) {

        return "Braeburn".equals(variety) ? Collections.singleton(new Apple(variety)) : Collections.emptySet();
    }
}
