package lross.doyoulikeapples.api;

import lross.doyoulikeapples.domain.Apple;

import java.util.Collection;

public interface AppleApi {

    /**
     * Gets all apples with {@link Apple#getVariety()} equal to the given variety.
     *
     * @param variety  an apple variety
     * @return  some apples
     */
    Collection<Apple> getApplesByVariety(String variety);
}
